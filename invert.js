// Function invert returns inverted keys and values of object object
function invert(object) {
    // Checks if input is object
    if (typeof object === "object") {
        // Initialize empty object to hold inverted ke value object
        let inverted_object = {};
        // Iterates through object keys
        for (let key in object) {
            inverted_object[`${String(object[key])}`] = key;
        }
        // Returns modified inverted object
        return inverted_object;
    }
    return "Check whether input is object or not";
}

// Exports function invert so that it can be imported in other files
module.exports = { invert };