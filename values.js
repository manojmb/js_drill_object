// Function values returns ivalues of object object in array
function values(object) {
    // Empty values_array to store Object object values
    let values_array = [];
    // Checks if input is object
    if (typeof object === "object") {
        for (let key in object) {
            // Iterates through object keys
            if (typeof object[key] != "function") {
                values_array.push(object[key]);
            }
        }
        // Returns array of string of values
        return values_array;
    }
    // Returns if input is not object
    return "Check whether input is object or not";
}

// Exports function values so that it can be imported in other files
module.exports = { values };
