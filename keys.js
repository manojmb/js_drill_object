// Function keys return array of keys of object object
function keys(object) {
    // Empty keys_array to store Object object keys
    let keys_array = [];
    // Checks if input is object
    if (typeof object === "object") {
        for (let key in object) {
            keys_array.push(key);
        }
        // Returns array of string of keys
        return keys_array;
    }
    // Returns if input is not object
    return "Check whether input is array or not";
}

// Exports function keys so that it can be imported in other files
module.exports = { keys };