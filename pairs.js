// Function pairs returns pair of keys and values array of Object object
function pairs(object) {
    // Empty pairs_array to store Object object pairs
    let pairs_array = [];
    // Checks if input is object
    if (typeof object === "object") {
        // Iterates through object keys
        for (let key in object) {
            pairs_array.push([key, object[key]]);
        }
        // Returns array of string of pairs
        return pairs_array;
    }
    // Returns if input is not object
    return "Check whether input is object or not";
}

// Exports function pairs so that it can be imported in other files
module.exports = { pairs };