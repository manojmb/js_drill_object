// Function defaults return defaults of object object
function defaults(object, defaultProps) {
    // Checks if input is object
    if (typeof object === "object") {
        // Iterates through object keys
        for (let key in defaultProps) {
            // Checks if key and value is missing 
            if (object[key] === undefined) {
                object[key] = defaultProps[key];
            }
        }
        // Returns modified object
        return object;
    }
}

// Exports function defaults so that it can be imported in other files
module.exports = { defaults };