// Function mapObject returns mapped values of object object
function mapObject(object, callback) {
    // Checks if input is object
    if (typeof object === "object" && typeof callback === "function") {
        // Iterates through object keys
        for (let key in object) {
            object[key] = callback(key, object[key]);
        }
        // Returns modified object
        return object;
    }
    // Returns if input is not object
    return "Check whether input is object and function";
}

// Exporting mapObject function so that it can be imported in other files
module.exports = { mapObject };