// Importing function defaults from file defaults.js
const { defaults } = require("../defaults.js");

// Object iceCream to check function defaults
let iceCream = { flavor: "chocolate" };

try {
    // Executes defaults function and returns modified filled object
    result = defaults(iceCream, { flavor: "vanilla", sprinkles: "lots" });
    console.log("\nObjects Defaults function test case:-");
    // Logs result on console
    console.log(result);
} catch (e) {
    // Logs message on console if encountered any
    console.log(e.message);
}