// Importing function invert from file invert.js
const { invert } = require("../invert.js");

// Object testObject to test function invert
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

try {
    // Executes invert function and returns modified inverted object
    result = invert(testObject);
    console.log("\nObjects Invert function test case:-");
    // Logs result on console
    console.log(result);
} catch (e) {
    // Logs message on console if encountered any
    console.log(e.message);
}