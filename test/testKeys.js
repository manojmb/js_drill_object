// Importing keys from keys.js
const { keys } = require("../keys.js");

// Object testObject to test function keys
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

try {
    // Executes keys function to get array of strings of keys of testObject
    result = keys(testObject);
    console.log("\nObjects keys function test case:-");
    // Logs result on console
    console.log(result);
} catch (e) {
    // Logs error message on console if encountered any
    console.log(e.message);
}