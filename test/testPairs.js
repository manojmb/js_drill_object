// Importing pairs from pairs.js
const { pairs } = require("../pairs.js");

// Object testObject to test function pairs
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

try {
    // Executes pairs function to get array of pairs of testObject
    result = pairs(testObject);
    console.log("\nObjects Pairs function test case:-");
    // Logs result on console
    console.log(result);
} catch (e) {
    // Logs error message on console if encountered any
    console.log(e.message);
}