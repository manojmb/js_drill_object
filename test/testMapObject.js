// Importing mapObject from mapObject.js
const { mapObject } = require("../mapObject.js");

// Object testObject to test function mapObject
const testObject = { start: 5, end: 12 };

try {
    // Executes mapObject function to get mapped object
    result = mapObject(testObject, function (key, val) {
        return val * 5;
    });
    console.log("\nObjects mapObject function test case:-");
    // Logs result on console
    console.log(result);
} catch (e) {
    // Logs error message on console if encountered any
    console.log(e.message);
}