// Importing values from values.js
const { values } = require("../values.js");

// Object testObject to test function values
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };


try {
    // Executes values function to get array of values of testObject
    result = values(testObject);
    console.log("\nObjects Values function test case:-");
    // Logs result on console
    console.log(result);
} catch (e) {
    // Logs error message on console if encountered any
    console.log(e.message);
}